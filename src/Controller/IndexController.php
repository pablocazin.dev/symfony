<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class IndexController extends AbstractController
{
    #[Route('/', name:'index_index',methods:['GET'])]
    public function index(ProductRepository $productRepository)
    {
        $context = array(
            "titre" => 'coucou',
            'products' => $productRepository->findAll(),
            'showEdit' => false
        );

        return $this->render('base.html.twig', $context);
    }
}